<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/',function(){
	return view("welcome");
});
Route::get('/login',function(){
	return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get("/getAllNoti", 'ManagerController@getAllNoti');

Route::get('/xemnoiquy', 'AdminController@xemnoiquy');

Route::post('/suanoiquy', 'AdminController@suanoiquy');

Route::post('/updatenoiquy', 'AdminController@updatenoiquy');

Route::get('/capnhatthongtin', 'AdminController@capnhatthongtin');

Route::post('/updatethongtin', 'AdminController@updatethongtin');

Route::get('/category','CategoryController@index');

Route::get('/category/new','CategoryController@new1');

Route::post('/category/new','CategoryController@create');

Route::get('/category/{id}','CategoryController@show');


Route::get('/category/delete/{id}','CategoryController@delete');
