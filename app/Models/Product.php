<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 May 2017 13:40:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Product
 * 
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $quantity
 * @property string $unit_calculate
 * @property float $price
 * @property int $resident_id
 * 
 * @property \App\Models\Resident $resident
 * @property \Illuminate\Database\Eloquent\Collection $orders
 *
 * @package App\Models
 */
class Product extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'quantity' => 'int',
		'price' => 'float',
		'resident_id' => 'int'
	];

	protected $fillable = [
		'name',
		'description',
		'quantity',
		'unit_calculate',
		'price',
		'resident_id'
	];

	public function resident()
	{
		return $this->belongsTo(\App\Models\Resident::class);
	}

	public function orders()
	{
		return $this->hasMany(\App\Models\Order::class);
	}
}
