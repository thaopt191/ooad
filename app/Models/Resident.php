<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 May 2017 13:40:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Resident
 * 
 * @property int $id
 * @property string $name
 * @property bool $owner
 * @property \Carbon\Carbon $birthday
 * @property int $user_id
 * @property int $apartment_id
 * 
 * @property \App\Models\Apartment $apartment
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $orders
 * @property \Illuminate\Database\Eloquent\Collection $products
 * @property \Illuminate\Database\Eloquent\Collection $reports
 *
 * @package App\Models
 */
class Resident extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'owner' => 'bool',
		'user_id' => 'int',
		'apartment_id' => 'int'
	];

	protected $dates = [
		'birthday'
	];

	protected $fillable = [
		'name',
		'owner',
		'birthday',
		'user_id',
		'apartment_id'
	];

	public function apartment()
	{
		return $this->belongsTo(\App\Models\Apartment::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function orders()
	{
		return $this->hasMany(\App\Models\Order::class);
	}

	public function products()
	{
		return $this->hasMany(\App\Models\Product::class);
	}

	public function reports()
	{
		return $this->hasMany(\App\Models\Report::class);
	}
}
