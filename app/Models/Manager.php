<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 May 2017 13:40:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Manager
 * 
 * @property int $id
 * @property int $user_id
 * @property string $name
 * 
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $announcements
 *
 * @package App\Models
 */
class Manager extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'name'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function announcements()
	{
		return $this->hasMany(\App\Models\Announcement::class);
	}
}
