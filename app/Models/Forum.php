<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Forum extends Model
{
	public $timestamps = true;
    protected $fillable = [
    	'id',
		'name',
		'description',
		'owner_id',
		'category_id',
		'locked'
	];
}
