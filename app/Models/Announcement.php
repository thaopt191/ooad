<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 May 2017 13:40:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Announcement
 * 
 * @property int $id
 * @property string $context
 * @property \Carbon\Carbon $time
 * @property bool $seen
 * @property int $manager_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Manager $manager
 * @property \App\Models\AnnouncementDetail $announcement_detail
 *
 * @package App\Models
 */
class Announcement extends Eloquent
{
	protected $casts = [
		'all' => 'bool',
		'owner_id' => 'int'
	];

	protected $fillable = [
		'header',
		'content',
		'all',
		'owner_id'
	];

	public function manager()
	{
		return $this->belongsTo(\App\Models\Manager::class);
	}

	public function announcement_detail()
	{
		return $this->hasOne(\App\Models\AnnouncementDetail::class);
	}
}
