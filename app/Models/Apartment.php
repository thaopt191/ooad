<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 May 2017 13:40:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Apartment
 * 
 * @property int $id
 * @property string $home_number
 * @property int $floor
 * @property string $description
 * @property float $acreage
 * 
 * @property \App\Models\AnnouncementDetail $announcement_detail
 * @property \Illuminate\Database\Eloquent\Collection $residents
 *
 * @package App\Models
 */
class Apartment extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'floor' => 'int',
		'acreage' => 'float'
	];

	protected $fillable = [
		'home_number',
		'floor',
		'description',
		'acreage'
	];

	public function announcement_detail()
	{
		return $this->hasOne(\App\Models\AnnouncementDetail::class);
	}

	public function residents()
	{
		return $this->hasMany(\App\Models\Resident::class);
	}
}
