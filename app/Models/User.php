<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 May 2017 13:40:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class User
 * 
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $phonenumber
 * @property bool $sex
 * @property string $password
 * @property string $role
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $admins
 * @property \Illuminate\Database\Eloquent\Collection $managers
 * @property \Illuminate\Database\Eloquent\Collection $residents
 *
 * @package App\Models
 */
class User extends Eloquent
{
	protected $casts = [
		'sex' => 'bool'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'name',
		'email',
		'phonenumber',
		'sex',
		'password',
		'role',
		'remember_token'
	];

	public function admins()
	{
		return $this->hasMany(\App\Models\Admin::class);
	}

	public function managers()
	{
		return $this->hasMany(\App\Models\Manager::class);
	}

	public function residents()
	{
		return $this->hasMany(\App\Models\Resident::class);
	}
}
