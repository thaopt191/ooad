<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 May 2017 13:40:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class AnnouncementDetail
 * 
 * @property int $announcement_id
 * @property int $apartment_id
 * @property string $note
 * 
 * @property \App\Models\Announcement $announcement
 * @property \App\Models\Apartment $apartment
 *
 * @package App\Models
 */
class AnnouncementDetail extends Eloquent
{
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'announcement_id' => 'int',
		'apartment_id' => 'int'
	];

	protected $fillable = [
		'announcement_id',
		'apartment_id',
		'note'
	];

	public function announcement()
	{
		return $this->belongsTo(\App\Models\Announcement::class);
	}

	public function apartment()
	{
		return $this->belongsTo(\App\Models\Apartment::class);
	}
}
