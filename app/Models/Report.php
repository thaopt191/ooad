<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 May 2017 13:40:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Report
 * 
 * @property int $id
 * @property string $context
 * @property bool $seen
 * @property int $resident_id
 * @property int $public_id
 * 
 * @property \App\Models\Public $public
 * @property \App\Models\Resident $resident
 *
 * @package App\Models
 */
class Report extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'seen' => 'bool',
		'resident_id' => 'int',
		'public_id' => 'int'
	];

	protected $fillable = [
		'context',
		'seen',
		'resident_id',
		'public_id'
	];

	public function public()
	{
		return $this->belongsTo(\App\Models\Public::class);
	}

	public function resident()
	{
		return $this->belongsTo(\App\Models\Resident::class);
	}
}
