<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 May 2017 13:40:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Public
 * 
 * @property int $id
 * @property string $name
 * @property string $phonenumber
 * @property string $address
 * 
 * @property \Illuminate\Database\Eloquent\Collection $reports
 *
 * @package App\Models
 */
class Public extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'name',
		'phonenumber',
		'address'
	];

	public function reports()
	{
		return $this->hasMany(\App\Models\Report::class);
	}
}
