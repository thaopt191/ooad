<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 15 May 2017 13:40:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Order
 * 
 * @property int $id
 * @property int $quantityOrder
 * @property string $note
 * @property bool $shipped
 * @property \Carbon\Carbon $time
 * @property int $resident_id
 * @property int $product_id
 * 
 * @property \App\Models\Product $product
 * @property \App\Models\Resident $resident
 *
 * @package App\Models
 */
class Order extends Eloquent
{
	public $timestamps = false;

	protected $casts = [
		'quantityOrder' => 'int',
		'shipped' => 'bool',
		'resident_id' => 'int',
		'product_id' => 'int'
	];

	protected $dates = [
		'time'
	];

	protected $fillable = [
		'quantityOrder',
		'note',
		'shipped',
		'time',
		'resident_id',
		'product_id'
	];

	public function product()
	{
		return $this->belongsTo(\App\Models\Product::class);
	}

	public function resident()
	{
		return $this->belongsTo(\App\Models\Resident::class);
	}
}
