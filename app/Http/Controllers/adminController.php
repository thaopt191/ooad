<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\regulation;
use App\Models\User;

class adminController extends Controller
{
    public function capnhatthongtin(Request $request) {
    	$user = User::where('id', 1)->first();

    	return view('capnhatthongtin')->with('user',$user); 
    }

    public function updatethongtin(Request $request) {
      //get data 
      $data = $request->all();

       $user = User::where('id', 1)->first();

       if($data['name'] != null)
           $user->name = $data['name'];
       if($data['password'] != null)
            $user->password = Hash::make($data['password']);
       
       $user->save();
       
      echo "thành công";
    }

    public function xemnoiquy(Request $request) {
      $noi_quys = regulation::all();
      
      return view('xemnoiquy')->with('noi_quys',$noi_quys);
    }

    public function suanoiquy(Request $request) {
      //get data 
      $data = $request->all();
      $id = $data['id'];

      $noi_quy = regulation::where('id', $id)->first();
      
      return view('suanoiquy')->with('noi_quy',$noi_quy); 
    }

    public function updatenoiquy(Request $request) {
    	$data = $request->all();
    	$id = $data['id'];
    	$noiquymoi = $data['noi_quy'];

    	$noi_quy = regulation::where('id', $id)->first();
    	$noi_quy->context = $noiquymoi;
    	$noi_quy->save();

    	echo "ok";
    }
}
