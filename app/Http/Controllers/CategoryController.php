<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Forum;

class CategoryController extends Controller
{
    public function index(){
    	$allCategory = Category::all();
    	return view("Forum/Category")->with('all',$allCategory);
    }

    public function show($id){
    	$category=Category::where('id',$id)->first();
    	$forumInCategory=Forum::where('category_id',$id)->get();
    	return view('Forum/show')->with(['category' => $category,'listForum' => $forumInCategory]);
    }

    public function new1(){
    	return view("Forum/new");
    }

    public function create(Request $request){
    	$category = new Category();
    	$category->name = $request->name;
    	$category->description = $request->description;
    	$category->save();

    	return redirect("/category");
    }

    public function delete($id){
    	$category=Category::find($id)->delete();
    	return redirect("/category");
    }
}
