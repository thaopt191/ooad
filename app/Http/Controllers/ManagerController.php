<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Announcement;
class ManagerController extends Controller
{
    public function getAllNoti()
    {
    	return json_encode(Announcement::all());
    }
}
