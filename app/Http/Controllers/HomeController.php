<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if(isset(Auth::user()->role)){
            $role = Auth::user()->role;
            if($role == "manager"){
                return view("manager.home_manager");
            }
            else if( $role == "resident"){
                return view("resident.home_resident");
            }
            else {
                return redirect("/login");
            }
        }
        else return redirect("/");
    }
}
