<?php

use Illuminate\Database\Seeder;
use App\Models\Forum;

class ForumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Forum::create([
        	'name'=>'Forum01',
        	'description'=>'Morbi malesuada lorem elit, eget iaculis leo maximus eu. Integer sollicitudin vehicula tellus. Integer rhoncus, orci at varius rhoncus, mi ex ullamcorper lorem, vitae lacinia nunc libero sit amet ante. Phasellus lobortis neque eget urna interdum, eu congue erat fermentum. Donec vel semper neque, id aliquam metus. Sed rhoncus sapien at libero posuere vehicula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut volutpat mauris vel ex feugiat cursus. Donec vitae vehicula massa, vitae posuere nulla. Curabitur quis tellus id diam viverra pulvinar. Proin eleifend nec ligula a lacinia. Curabitur ex tellus, fringilla pellentesque quam nec, pellentesque ullamcorper odio. Nullam dictum ac nulla varius tempus. Pellentesque eget finibus nisi, blandit viverra metus. Donec finibus ipsum et faucibus sollicitudin. ',
        	'owner_id'=>'1',
        	'category_id'=>'1',
        	'locked'=>'0'
        ]);
        Forum::create([
        	'name'=>'Forum01',
        	'description'=>'Morbi malesuada lorem elit, eget iaculis leo maximus eu. Integer sollicitudin vehicula tellus. Integer rhoncus, orci at varius rhoncus, mi ex ullamcorper lorem, vitae lacinia nunc libero sit amet ante. Phasellus lobortis neque eget urna interdum, eu congue erat fermentum. Donec vel semper neque, id aliquam metus. Sed rhoncus sapien at libero posuere vehicula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut volutpat mauris vel ex feugiat cursus. Donec vitae vehicula massa, vitae posuere nulla. Curabitur quis tellus id diam viverra pulvinar. Proin eleifend nec ligula a lacinia. Curabitur ex tellus, fringilla pellentesque quam nec, pellentesque ullamcorper odio. Nullam dictum ac nulla varius tempus. Pellentesque eget finibus nisi, blandit viverra metus. Donec finibus ipsum et faucibus sollicitudin. ',
        	'owner_id'=>'1',
        	'category_id'=>'1',
        	'locked'=>'0'
        ]);
        Forum::create([
        	'name'=>'Forum02',
        	'description'=>'Morbi malesuada lorem elit, eget iaculis leo maximus eu. Integer sollicitudin vehicula tellus. Integer rhoncus, orci at varius rhoncus, mi ex ullamcorper lorem, vitae lacinia nunc libero sit amet ante. Phasellus lobortis neque eget urna interdum, eu congue erat fermentum. Donec vel semper neque, id aliquam metus. Sed rhoncus sapien at libero posuere vehicula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut volutpat mauris vel ex feugiat cursus. Donec vitae vehicula massa, vitae posuere nulla. Curabitur quis tellus id diam viverra pulvinar. Proin eleifend nec ligula a lacinia. Curabitur ex tellus, fringilla pellentesque quam nec, pellentesque ullamcorper odio. Nullam dictum ac nulla varius tempus. Pellentesque eget finibus nisi, blandit viverra metus. Donec finibus ipsum et faucibus sollicitudin. ',
        	'owner_id'=>'1',
        	'category_id'=>'1',
        	'locked'=>'0'
        ]);
        Forum::create([
        	'name'=>'Forum03',
        	'description'=>'Morbi malesuada lorem elit, eget iaculis leo maximus eu. Integer sollicitudin vehicula tellus. Integer rhoncus, orci at varius rhoncus, mi ex ullamcorper lorem, vitae lacinia nunc libero sit amet ante. Phasellus lobortis neque eget urna interdum, eu congue erat fermentum. Donec vel semper neque, id aliquam metus. Sed rhoncus sapien at libero posuere vehicula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut volutpat mauris vel ex feugiat cursus. Donec vitae vehicula massa, vitae posuere nulla. Curabitur quis tellus id diam viverra pulvinar. Proin eleifend nec ligula a lacinia. Curabitur ex tellus, fringilla pellentesque quam nec, pellentesque ullamcorper odio. Nullam dictum ac nulla varius tempus. Pellentesque eget finibus nisi, blandit viverra metus. Donec finibus ipsum et faucibus sollicitudin. ',
        	'owner_id'=>'1',
        	'category_id'=>'1',
        	'locked'=>'0'
        ]);
        Forum::create([
        	'name'=>'Forum04',
        	'description'=>'Morbi malesuada lorem elit, eget iaculis leo maximus eu. Integer sollicitudin vehicula tellus. Integer rhoncus, orci at varius rhoncus, mi ex ullamcorper lorem, vitae lacinia nunc libero sit amet ante. Phasellus lobortis neque eget urna interdum, eu congue erat fermentum. Donec vel semper neque, id aliquam metus. Sed rhoncus sapien at libero posuere vehicula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Ut volutpat mauris vel ex feugiat cursus. Donec vitae vehicula massa, vitae posuere nulla. Curabitur quis tellus id diam viverra pulvinar. Proin eleifend nec ligula a lacinia. Curabitur ex tellus, fringilla pellentesque quam nec, pellentesque ullamcorper odio. Nullam dictum ac nulla varius tempus. Pellentesque eget finibus nisi, blandit viverra metus. Donec finibus ipsum et faucibus sollicitudin. ',
        	'owner_id'=>'1',
        	'category_id'=>'2',
        	'locked'=>'0'
        ]);
    }
}
