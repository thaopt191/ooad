<?php

use Illuminate\Database\Seeder;

use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'manager',
            'email' => 'manager@gmail.com',
            'phonenumber' => '0909090909',
            'password' => Hash::make('quanli'),
            'sex'=> true,
            'role'=> 'manager',
        ]);
        
        User::create([
            'name' => 'Hiếu',
            'email' => 'hieu@gmail.com',    
            'phonenumber' => '0909090909',
            'password' => Hash::make('quanli'),
            'sex'=> true,
            'role'=> 'resident',
        ]);

        User::create([
            'name' => 'Thảo',
            'email' => 'thao@gmail.com',
            'phonenumber' => '0909090909',
            'password' => Hash::make('quanli'),
            'sex'=> true,
            'role'=> 'resident',
        ]);

        User::create([
            'name' => 'Toàn',
            'email' => 'toan@gmail.com',
            'phonenumber' => '0909090909',
            'password' => Hash::make('quanli'),
            'sex'=> true,
            'role'=> 'resident',
        ]);

        User::create([
            'name' => 'Tuấn',
            'email' => 'tuan@gmail.com',
            'phonenumber' => '0909090909',
            'password' => Hash::make('quanli'),
            'sex'=> true,
            'role'=> 'resident',
        ]);

        User::create([
            'name' => 'Phi',
            'email' => 'fizz@gmail.com',
            'phonenumber' => '0909090909',
            'password' => Hash::make('quanli'),
            'sex'=> true,
            'role'=> 'resident',
        ]);
    }
}
