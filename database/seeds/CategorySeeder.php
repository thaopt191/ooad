<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
        	'name' => 'Category01',
        	'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla sem sed urna eleifend placerat. Curabitur convallis sagittis massa. Nullam efficitur diam vitae est dignissim, nec maximus odio facilisis. Aenean interdum, nunc nec aliquet rutrum, ipsum massa elementum tortor, molestie ornare nunc dui eget ante. Aenean vestibulum interdum urna, vitae imperdiet leo sodales maximus. Donec arcu elit, egestas in dui quis, gravida malesuada mauris. Suspendisse potenti. Vivamus lectus purus, sodales id lacus a, eleifend scelerisque ligula. Praesent et felis fermentum, mattis nibh ut, tempus mi. ',
        ]);

        Category::create([
        	'name' => 'Category02',
        	'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla sem sed urna eleifend placerat. Curabitur convallis sagittis massa. Nullam efficitur diam vitae est dignissim, nec maximus odio facilisis. Aenean interdum, nunc nec aliquet rutrum, ipsum massa elementum tortor, molestie ornare nunc dui eget ante. Aenean vestibulum interdum urna, vitae imperdiet leo sodales maximus. Donec arcu elit, egestas in dui quis, gravida malesuada mauris. Suspendisse potenti. Vivamus lectus purus, sodales id lacus a, eleifend scelerisque ligula. Praesent et felis fermentum, mattis nibh ut, tempus mi. ',
        ]);
        Category::create([
        	'name' => 'Category03',
        	'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla sem sed urna eleifend placerat. Curabitur convallis sagittis massa. Nullam efficitur diam vitae est dignissim, nec maximus odio facilisis. Aenean interdum, nunc nec aliquet rutrum, ipsum massa elementum tortor, molestie ornare nunc dui eget ante. Aenean vestibulum interdum urna, vitae imperdiet leo sodales maximus. Donec arcu elit, egestas in dui quis, gravida malesuada mauris. Suspendisse potenti. Vivamus lectus purus, sodales id lacus a, eleifend scelerisque ligula. Praesent et felis fermentum, mattis nibh ut, tempus mi. ',
        ]);
        Category::create([
        	'name' => 'Category04',
        	'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla sem sed urna eleifend placerat. Curabitur convallis sagittis massa. Nullam efficitur diam vitae est dignissim, nec maximus odio facilisis. Aenean interdum, nunc nec aliquet rutrum, ipsum massa elementum tortor, molestie ornare nunc dui eget ante. Aenean vestibulum interdum urna, vitae imperdiet leo sodales maximus. Donec arcu elit, egestas in dui quis, gravida malesuada mauris. Suspendisse potenti. Vivamus lectus purus, sodales id lacus a, eleifend scelerisque ligula. Praesent et felis fermentum, mattis nibh ut, tempus mi. ',
        ]);
    }
}
