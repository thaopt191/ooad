<?php

use Illuminate\Database\Seeder;
use App\Models\Announcement;

class AnnouncementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Announcement::create([
        	'header' => 'Họp dân phố',
        	'content' => 'THÔNG BÁO

Về việc họp tổ dân phố

Trong những ngày đầu năm, do việc tổ chức lễ hội nên tình hình tổ dân phố có một số hiện tượng gây rối làm mất trật tự an toàn xã hội. Vậy kính mời đại diện các hộ gia đình tổ dân phố đến họp để bàn về một số biện pháp đảm bảo an ninh trật tự khu dân cư.

Thời gian: 20h 00’ ngày 18/03/2017

Địa điểm: nhà ông tổ trưởng tổ dân phố số 206, phố Thanh Sơn.

Kính mong các ông (bà) đến đầy đủ và đúng giờ.

Thông báo này thay cho giấy mời.

                                         T/M Ban quản lí tổ dân phố

                                         Tổ trưởng

                                         Nam

                                         Vũ Văn Nam',
            'all' => '1',
            'owner_id' => '1'
        ]);
        Announcement::create([
        	'header' => 'Họp dân phố',
        	'content' => 'THÔNG BÁO

Về việc họp tổ dân phố

Trong những ngày đầu năm, do việc tổ chức lễ hội nên tình hình tổ dân phố có một số hiện tượng gây rối làm mất trật tự an toàn xã hội. Vậy kính mời đại diện các hộ gia đình tổ dân phố đến họp để bàn về một số biện pháp đảm bảo an ninh trật tự khu dân cư.

Thời gian: 20h 00’ ngày 18/03/2017

Địa điểm: nhà ông tổ trưởng tổ dân phố số 206, phố Thanh Sơn.

Kính mong các ông (bà) đến đầy đủ và đúng giờ.

Thông báo này thay cho giấy mời.

                                         T/M Ban quản lí tổ dân phố

                                         Tổ trưởng

                                         Nam

                                         Vũ Văn Nam',
            'all' => '1',
            'owner_id' => '1'
        ]);
        Announcement::create([
        	'header' => 'Họp dân phố',
        	'content' => 'THÔNG BÁO

Về việc họp tổ dân phố

Trong những ngày đầu năm, do việc tổ chức lễ hội nên tình hình tổ dân phố có một số hiện tượng gây rối làm mất trật tự an toàn xã hội. Vậy kính mời đại diện các hộ gia đình tổ dân phố đến họp để bàn về một số biện pháp đảm bảo an ninh trật tự khu dân cư.

Thời gian: 20h 00’ ngày 18/03/2017

Địa điểm: nhà ông tổ trưởng tổ dân phố số 206, phố Thanh Sơn.

Kính mong các ông (bà) đến đầy đủ và đúng giờ.

Thông báo này thay cho giấy mời.

                                         T/M Ban quản lí tổ dân phố

                                         Tổ trưởng

                                         Nam

                                         Vũ Văn Nam',
            'all' => '1',
            'owner_id' => '1'
        ]);
        Announcement::create([
        	'header' => 'Họp dân phố',
        	'content' => 'THÔNG BÁO

Về việc họp tổ dân phố

Trong những ngày đầu năm, do việc tổ chức lễ hội nên tình hình tổ dân phố có một số hiện tượng gây rối làm mất trật tự an toàn xã hội. Vậy kính mời đại diện các hộ gia đình tổ dân phố đến họp để bàn về một số biện pháp đảm bảo an ninh trật tự khu dân cư.

Thời gian: 20h 00’ ngày 18/03/2017

Địa điểm: nhà ông tổ trưởng tổ dân phố số 206, phố Thanh Sơn.

Kính mong các ông (bà) đến đầy đủ và đúng giờ.

Thông báo này thay cho giấy mời.

                                         T/M Ban quản lí tổ dân phố

                                         Tổ trưởng

                                         Nam

                                         Vũ Văn Nam',
            'all' => '0',
            'owner_id' => '1'
        ]);
        Announcement::create([
        	'header' => 'Họp dân phố',
        	'content' => 'THÔNG BÁO

Về việc họp tổ dân phố

Trong những ngày đầu năm, do việc tổ chức lễ hội nên tình hình tổ dân phố có một số hiện tượng gây rối làm mất trật tự an toàn xã hội. Vậy kính mời đại diện các hộ gia đình tổ dân phố đến họp để bàn về một số biện pháp đảm bảo an ninh trật tự khu dân cư.

Thời gian: 20h 00’ ngày 18/03/2017

Địa điểm: nhà ông tổ trưởng tổ dân phố số 206, phố Thanh Sơn.

Kính mong các ông (bà) đến đầy đủ và đúng giờ.

Thông báo này thay cho giấy mời.

                                         T/M Ban quản lí tổ dân phố

                                         Tổ trưởng

                                         Nam

                                         Vũ Văn Nam',
            'all' => '0',
            'owner_id' => '1'
        ]);
        Announcement::create([
        	'header' => 'Họp dân phố',
        	'content' => 'THÔNG BÁO

Về việc họp tổ dân phố

Trong những ngày đầu năm, do việc tổ chức lễ hội nên tình hình tổ dân phố có một số hiện tượng gây rối làm mất trật tự an toàn xã hội. Vậy kính mời đại diện các hộ gia đình tổ dân phố đến họp để bàn về một số biện pháp đảm bảo an ninh trật tự khu dân cư.

Thời gian: 20h 00’ ngày 18/03/2017

Địa điểm: nhà ông tổ trưởng tổ dân phố số 206, phố Thanh Sơn.

Kính mong các ông (bà) đến đầy đủ và đúng giờ.

Thông báo này thay cho giấy mời.

                                         T/M Ban quản lí tổ dân phố

                                         Tổ trưởng

                                         Nam

                                         Vũ Văn Nam',
            'all' => '1',
            'owner_id' => '1'
        ]);
        Announcement::create([
        	'header' => 'Họp dân phố',
        	'content' => 'THÔNG BÁO

Về việc họp tổ dân phố

Trong những ngày đầu năm, do việc tổ chức lễ hội nên tình hình tổ dân phố có một số hiện tượng gây rối làm mất trật tự an toàn xã hội. Vậy kính mời đại diện các hộ gia đình tổ dân phố đến họp để bàn về một số biện pháp đảm bảo an ninh trật tự khu dân cư.

Thời gian: 20h 00’ ngày 18/03/2017

Địa điểm: nhà ông tổ trưởng tổ dân phố số 206, phố Thanh Sơn.

Kính mong các ông (bà) đến đầy đủ và đúng giờ.

Thông báo này thay cho giấy mời.

                                         T/M Ban quản lí tổ dân phố

                                         Tổ trưởng

                                         Nam

                                         Vũ Văn Nam',
            'all' => '1',
            'owner_id' => '1'
        ]);
        Announcement::create([
        	'header' => 'Họp dân phố',
        	'content' => 'THÔNG BÁO

Về việc họp tổ dân phố

Trong những ngày đầu năm, do việc tổ chức lễ hội nên tình hình tổ dân phố có một số hiện tượng gây rối làm mất trật tự an toàn xã hội. Vậy kính mời đại diện các hộ gia đình tổ dân phố đến họp để bàn về một số biện pháp đảm bảo an ninh trật tự khu dân cư.

Thời gian: 20h 00’ ngày 18/03/2017

Địa điểm: nhà ông tổ trưởng tổ dân phố số 206, phố Thanh Sơn.

Kính mong các ông (bà) đến đầy đủ và đúng giờ.

Thông báo này thay cho giấy mời.

                                         T/M Ban quản lí tổ dân phố

                                         Tổ trưởng

                                         Nam

                                         Vũ Văn Nam',
            'all' => '1',
            'owner_id' => '1'
        ]);
    }
}
