<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(UsersTableSeeder::class);
        //$this->call(ManagerSeeder::class);
        //$this->call(AnnouncementSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(ForumSeeder::class);
    }
}
