<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('reports', function (Blueprint $table){
            $table->increments('id');
            $table->string('context');
            $table->boolean('seen');
            $table->integer('resident_id')->unsigned();
            $table->integer('public_id')->unsigned();

            $table->foreign('resident_id')
                ->references('id')
                ->on('residents')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('public_id')
                ->references('id')
                ->on('publics')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('reports');
    }
}
