<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnnouncementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('announcements', function (Blueprint $table){
            $table->increments('id');
            $table->string('header');

            $table->string('content',9000);
            $table->boolean('all');

            $table->integer('owner_id')->unsigned();

            $table->foreign('owner_id')
                ->references('id')
                ->on('managers')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('announcements');
    }
}
