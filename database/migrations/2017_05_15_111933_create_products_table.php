<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('products', function (Blueprint $table){
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->integer('quantity');
            $table->string('unit_calculate');
            $table->double('price');
            $table->integer('resident_id')->unsigned();
            $table->foreign('resident_id')
                ->references('id')
                ->on('residents')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('products');
    }
}
