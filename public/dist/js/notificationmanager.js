function startNotificationManager(){
	clickThongbaochung();
}

function clickThongbaochung(){
	$("#thongbaochung").click(function(){
			$.get("/getAllNoti",function(data, status){
				if(status == "success"){
					$("#main-content-div").empty();
					$content = '<div class="row"><div class="col-md-6"><div class="portlet box grey-cascade">\
						<div class="portlet-title">\
							<div class="caption">\
								<i class="fa fa-globe"></i>Thông báo\
							</div>\
							<div class="tools">\
								<a href="javascript:;" class="collapse">\
								</a>\
								<a href="#portlet-config" data-toggle="modal" class="config">\
								</a>\
								<a href="javascript:;" class="reload">\
								</a>\
								<a href="javascript:;" class="remove">\
								</a>\
							</div>\
						</div>\
						<div class="portlet-body">\
							<div class="table-toolbar">\
								<div class="row">\
									<div class="col-md-6">\
										<div class="btn-group">\
											<button id="sample_editable_1_new" class="btn green">\
											Add New <i class="fa fa-plus"></i>\
											</button>\
										</div>\
									</div>\
									<div class="col-md-6">\
										<div class="btn-group pull-right">\
											<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>\
											</button>\
											<ul class="dropdown-menu pull-right">\
												<li>\
													<a href="javascript:;">\
													Print </a>\
												</li>\
												<li>\
													<a href="javascript:;">\
													Save as PDF </a>\
												</li>\
												<li>\
													<a href="javascript:;">\
													Export to Excel </a>\
												</li>\
											</ul>\
										</div>\
									</div>\
								</div>\
							</div>\
							<table class="table table-striped table-bordered table-hover" id="sample_1">\
							<thead>\
							<tr>\
								<th>#</th>\
								<th>\
									 Tiêu đề\
								</th>\
								<th>\
									 Cập nhật\
								</th>\
								<th>\
									 Đối tượng\
								</th>\
							</tr>\
							</thead>\
							<tbody>';
						$obj = JSON.parse(data);
						for($i = 0; $i < $obj.length ; $i++){
							$content += "<tr class='row-noti'>";
							$content += "<td>" + $obj[$i].id + "</td>";
							$content += "<td>" + $obj[$i].context + "</td>";
							$content += "<td>" + $obj[$i].time +"</td>";
							if($obj[$i].seen){
								$content += "<td>" + "Toàn thể" + " </td>";

							} 
							else {
								$content += "<td>" + "Cá nhân" + "</td>";
							}
							$content += "</tr>";
						}

						$content +='</tbody>\
							</table>\
						</div>\
					</div></div>\
					<div class="col-md-6" id="content-noti"></div> \
					</div>';
					$("#main-content-div").append($content);
					$("#sample_1").DataTable();
					clickRowNoti();
				}
				else{
					createAlert('danger', "Đã có lỗi xảy ra, vui lòng thử lại sau!");
				}
			});
	});
}
function clickRowNoti(){
	$(".row-noti").click(function(){
		$('#loading-gif').remove();
		$html = "<img style='margin-top:100px;margin-left:200px' src ='dist/loading.gif' id='loading-gif' width='100' height='100'  ></img>"
		$("#content-noti").append($html);
		$('#loading-gif').delay(2000).fadeOut('fast');
	});
}