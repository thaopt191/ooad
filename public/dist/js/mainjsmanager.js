
$(document).ready(function(){
	ajaxLoading();
	ajaxSetup();
    startNotificationManager();
	
});

function ajaxLoading(){
	 $(document).ajaxStart(function(){
        $("#wait").remove();
        $html = '<div id="wait" class="alert alert-success" style="z-index: 1000; position:fixed;bottom:10px;right:10px;">\
                  <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>\
                  <i class="fa fa-refresh fa-spin"></i>\
                  <strong>Đang xử lý...</strong>\
                </div>';
        //Pace.restart();
        $('#main-content').append($html);
    });
    $(document).ajaxComplete(function(){
        $("#wait").remove();
        console.log("ajaxComplete");
    });
}
function ajaxSetup(){
	$.ajaxSetup({
            headers:
            { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
        });
}