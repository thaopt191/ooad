@extends('manager.template_manager')

@section('menu-content')
<li class="start ">
					<a href="javascript:;">
					<i class="fa fa-bell-o"></i>
					<span class="title">Thông báo</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="#" id="thongbaochung">
							<i class="icon-bar-chart"></i>
							 Thông báo chung</a>
						</li>
						<li>
							<a href="#" id="yeucauguilen">
							<i class="icon-bulb"></i>
							Yêu cầu gửi lên</a>
						</li>
						
					</ul>
</li>
@endsection

@section('main-content')
	<div class="Category-list">
		@foreach ($all as $cate)
		    <a style="font-size: 20px;" href='/category/{{$cate->id}}'>-{{ $cate->name }}</a></br>
		    <span style="font-style: italic; padding-left: 30px;"> đăng tải lúc: {{ $cate->updated_at }}</span></br>
		    </hr>
		@endforeach
	</div>
@endsection
