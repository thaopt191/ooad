@extends('manager.template_manager')

@section('main-content')
	<div class="new-category">
		<p id="msg"></p>
		{!! Form::open(["id"=>"form-create"]) !!}
			<h1>Tạo một Category mới</h1>
 			{!! Form::label('contentlabel', 'Tên')!!}</br>
 			{!! Form::text('name')!!}</br>
 			{!! Form::label('descriptionlabel', 'Mô tả')!!}</br>
 			{!! Form::textarea('description')!!}</br>
			<button class="btn btn-primary" id="create-btn">Tạo</button>
		{!! Form::close() !!}
	</div>
@endsection
