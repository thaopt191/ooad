@extends('manager.template_manager')

@section('menu-content')
<li class="start ">
					<a href="javascript:;">
					<i class="fa fa-bell-o"></i>
					<span class="title">Thông báo</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
							<a href="#" id="thongbaochung">
							<i class="icon-bar-chart"></i>
							 Thông báo chung</a>
						</li>
						<li>
							<a href="#" id="yeucauguilen">
							<i class="icon-bulb"></i>
							Yêu cầu gửi lên</a>
						</li>
						
					</ul>
</li>
@endsection

@section('main-content')
đây là trang manager
@endsection

@section('js-import')
	<script type="text/javascript" src="dist/js/mainjsmanager.js"></script>
	<script type="text/javascript" src="dist/js/notificationmanager.js"></script>
	<script type="text/javascript" src="dist/api.js"></script>
	<script src="../../assets/admin/pages/scripts/table-managed.js"></script>
	<script>
jQuery(document).ready(function() {       
  // init demo features
   TableManaged.init();
});
</script>
@endsection

@section('username')
	
@endsection