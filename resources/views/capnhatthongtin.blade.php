@extends('layouts.app')

@section('content')
<html>
<div id = "container">
	<form action = "updatethongtin" method="post" id="form" class ="form-horizontal col-lg-8">
        <div class="panel panel-flat">
            <div class="panel-heading">
                    <h5 class="panel-title">Cập nhật thông tin tài khoản</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                <div class="form-group">
                    <label class="control-label col-lg-3">Tên đăng nhập:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="name" value="{{$user->name}}">
                        </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-3">Mật khẩu:</label>
                        <div class="col-lg-9">
                            <input type="password" class="form-control" name="password" value="{{$user->password}}">
                        </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-3">Số điện thoại:</label>
                        <div class="col-lg-9">
                            <input type="text" class="form-control" name="phone">
                        </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-3">Email:</label>
                        <div class="col-lg-9">
                            <input type="email" class="form-control" name="email">
                        </div>
                </div>               
        		
         		
        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
            
            	<button class="btn btn-primary" name="updatethongtin" id="test">Cập nhật <i class="icon-arrow-right14 position-right"></i></button>
	        </div>
       </div>
    </form>

<div id = "err"></div>
</div>	
</html>

<!-- <script type="text/javascript">
	$("button#test").click(function(e){
    e.preventDefault();

    var form_data = new FormData();

    var username = $('input[name=username]').val();
   form_data.append('username', username);

    var password = $('input[name=password]').val();
    form_data.append('password', password);
      
    var email = $('input[name=email]').val();
    form_data.append('email', email);

    var phone = $('input[name=phone]').val();
    form_data.append('phone', phone);

    var token = $('input[name=_token]').val();
    form_data.append('_token', token);
            var url=$(this).attr('name');
            
            $.ajax({
            type : 'POST', 
            url  : url,
            data: form_data,
            processData: false,
            contentType: false,
            
            success :  function(data){
                    $('#err').empty();

                   $('div#container').empty().html(data);
            },
            error: function(data){
              var errors = data.responseJSON;

              $.each(errors, function(index, value) {
                   $('#err').append("<p> *"+value +"</p>");
              }); 
            }
            })
  });
</script> -->
@endsection
