@extends('manager.template_manager')

@section('content')
<html>
<div id = "container">
	<form method = "post" action = "updatenoiquy" id="form" class ="form-horizontal col-lg-8">
        <div class="panel panel-flat">
            <div class="panel-heading">
                    <h5 class="panel-title">Cập nhật nội quy:</h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">

                <div class="form-group">
                  <label class="control-label col-lg-3">Nội quy:</label>
                  <div class="col-lg-9">
                    <textarea rows="5" cols="5" name="noi_quy" class="form-control" placeholder="Nhập nội quy:">{{$noi_quy->context}}</textarea>
                  </div>
                </div>
        		
        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value="{{$noi_quy->id}}" id="id"/>
            	<button class="btn btn-primary" name="updatenoiquy" id="test">Cập nhật</button>
	        </div>
       </div>
    </form>

<div id = "err"></div>
</div>	
</html>

<!-- <script type="text/javascript">
	$("button#test").click(function(e){
    e.preventDefault();

    var form_data = new FormData();

    var noi_quy = $('#noi_quy').val();
    form_data.append('noi_quy', noi_quy);

    var id = $('#id').val();
    form_data.append('id', id);

    var token = $('input[name=_token]').val();
    form_data.append('_token', token);
            var url=$(this).attr('name');
            
            $.ajax({
            type : 'POST', 
            url  : 'updatenoiquy',
            data: form_data,
            processData: false,
            contentType: false,
            
            success :  function(data){
                    $('#err').empty();

                   $('div#container').empty().html(data);
            },
            error: function(data){
              var errors = data.responseJSON;

              $.each(errors, function(index, value) {
                   $('#err').append("<p> *"+value +"</p>");
              }); 
            }
            })
  });
</script> -->
@endsection
